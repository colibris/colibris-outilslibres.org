import React, { useRef, useEffect } from 'react'
import { siteMetadata } from '../../gatsby-config'
import ColibrisNavi from '../components/ColibrisNavi'
import SiteNavi from '../components/SiteNavi'
import SiteFooter from '../components/SiteFooter'
import '../sass/gatstrap.scss'
import 'animate.css/animate.css'
import 'font-awesome/css/font-awesome.css'

class Template extends React.Component {
  observeEmergence() {
    if (!!window.IntersectionObserver) {
      let observer = new IntersectionObserver(
        (entries, observer) => {
          entries.forEach(entry => {
            if (entry.isIntersecting) {
              //console.log(entry);
              entry.target.setAttribute('data-emergence', 'visible');
              observer.unobserve(entry.target);
            }
          });
        },
        { rootMargin: "0px 0px 0px 0px" }
      );
      document.querySelectorAll("[data-emergence]").forEach(item => {
        observer.observe(item);
      });
    }
  }
  componentDidMount() {
    this.observeEmergence();
  }

  componentDidUpdate() {
    this.observeEmergence();
  }

  render() {
    const { location, children } = this.props
    return (
      <>
        <ColibrisNavi {...this.props} />
        <SiteNavi title={siteMetadata.title} {...this.props} />
        {children}
        <SiteFooter />
      </>
    )
  }
}

export default Template

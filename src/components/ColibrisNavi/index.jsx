import React from 'react'
import './style.scss'

class ColibrisNavi extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      menu: {}
    }
  }
  componentDidUpdate() {
    document.getElementsByTagName("body")[0].scrollTo({
      top: 0,
      left: 0,
      behavior: 'auto'
    });
  }

  componentDidMount() {
    fetch('https://colibris-lemouvement.org/archipel-markup?domain=colibris-outilslibres.org')
      .then( (response) => {
        return response.json()
      })
      .then( (json) => {
        this.setState({
          menu: json
        })
      });
  }

  render() {
      return (
        <>
          <style>{this.state.menu.style}</style>
          <div id="archipel-colibris"
            dangerouslySetInnerHTML={{ __html: this.state.menu.markup }}
          />
                    <center><a href="https://www.colibris-lemouvement.org/mooc-revolutions-locales/" style={{display:'block'}}>
            <img className="img-responsive" src="https://www.colibris-lemouvement.org/sites/all/themes/colibris/images/banner_mooc_revolutions_locales.jpg"
           alt="Découvrez le MOOC (R)évolutions Locales pour s'engager collectivement sur son territoire" />
          </a></center>

        </>
      );
  }
}

export default ColibrisNavi

import React from 'react'
import Link from 'gatsby-link'
import logo from './img/Logo_Colibris_Blanc_RVB.svg'; // Tell Webpack this JS file uses this image
import chatons from './img/chatons.svg'; // Tell Webpack this JS file uses this image
import './style.scss'

class SiteFooter extends React.Component {
  render() {
    return (
      <footer className="footer" role="banner">
        <div className="container">     
          <div className="row">
            <div className="col-xl-2 col-md-2 col-4">
              <a href="https://chatons.org/" title="Le site des chatons"><img src={chatons} alt="Le site des chatons" /></a>
            </div>
            <div className="col-xl-3 col-md-3 col-8">
              <ul className="menu">
                <li><a href="https://chatons.org/" title="Le site des chatons">Le site des chatons</a></li>
                <li><a href="https://degooglisons-internet.org/liste" title="Un site de Framasoft : Dégooglisons Internet">Liste de services libres alternatifs</a></li>
                <li><a href="https://colibris-wiki.org/chatons" title="Espace technique des Colibris">Espace technique des Colibris</a></li>
                <li><a href="https://framagit.org/colibris" title="Code source des services libres colibris">Code source des services libres colibris</a></li>
              </ul>  
            </div>

            <div className="col-xl-3 col-md-3 col-4">
              <ul className="menu">
                <li>
                  <Link to="/contact">
                  Contact
                  </Link>
                </li>
                <li>
                  <Link to="/mentions-legales">
                    Mentions légales
                  </Link>
                </li>
                <li>
                  <Link to="/conditions-generales-d-utilisation">
                    Conditions générales d'utilisation
                  </Link>
                </li>
                <li>
                  <a href="https://status.colibris-outilslibres.org/" title="Etat des services">
                    Etat des services
                  </a>
                </li>          
              </ul>  
            </div>

            <div className="col-xl-2 col-md-2 col-4">
              <ul className="menu">
                <li><a href="https://www.colibris-lemouvement.org/" title="Le site du mouvement">Colibris</a></li>
                <li><a href="https://www.colibris-universite.org/" title="L'université">L'université</a></li>
                <li><a href="https://www.colibris-lafabrique.org/" title="La fabrique citoyenne des Colibris">La fabrique</a></li>
                <li><a href="https://presdecheznous.fr/" title="Près de chez nous">Près de chez nous</a></li>
                <li><a href="https://www.colibris-laboutique.org/" title="La boutique">La boutique</a></li>
                <li><a href="https://www.jedonneenligne.org/colibris/OUTILSLIBRES/" title="Nous soutenir">Nous soutenir</a></li>
              </ul>  
            </div>

            <div className="col-xl-2 col-md-2 col-4 text-right">
              <a href="https://www.colibris-lemouvement.org/" title="Le site du mouvement">
                <img src={logo} alt="logo droite" />
              </a>
            </div>
          </div>
        </div>
      </footer>
    )
  }
}

export default SiteFooter

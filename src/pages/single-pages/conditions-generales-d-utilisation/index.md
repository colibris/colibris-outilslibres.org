---
title: "Conditions générales d'utilisation"
date: "2018-01-29T22:12:03.284Z"
layout: page
path: "/conditions-generales-d-utilisation/"
---

## Préambule

Les présentes conditions générales décrivent les termes et conditions applicables aux services proposés par le nom de domaine et ces sous domaine colibris-outilslibres.org.  
En utilisant les service proposés par l'association Colibris, vous acceptez d’être lié par les conditions suivantes.  
La liste des outils proposés est disponible sur la page d’accueil.  
Les présentes Conditions Générales prennent effet à compter de leur publication sur le site et restent en vigueur jusqu'à leur modification partielle ou totale par l’association. L’association se réserve le droit de modifier les Conditions Générales à tout moment. Chaque utilisation des services proposés sur le site constitue l'acceptation de la dernière version des Conditions Générales publiées sur le site.  

## Objet

Le site colibris-outilslibres.org a pour objet la fourniture d’outils libres sous formes de services facilitant la coopération à distance et le partage de connaissances.   

## En résumé

Les CGU n’étant pas toujours très explicite, voici un résumé de l’intention que nous souhaitons insuffler en vous proposant nos outils-libres :  

*   Votre contenu vous appartient.
*   Nous n’exploitons pas vos données personnelles sauf à fin de statistiques internes (anonymisées) ou pour vous prévenir d’un changement important sur le service.
*   Nous ne transmettons ni ne revendons pas vos données personnelles.
*   Nous sommes un mouvement citoyen par des valeurs de solidarité et d'humanisme. Pour plus d'informations : [https://www.colibris-lemouvement.org/mouvement/nos-valeurs](https://www.colibris-lemouvement.org/mouvement/nos-valeurs)
*   Nous ne garantissons aucune qualité de service. le site peux être fermé ou vos données perdues par notre ou votre faute. Nous sommes pour la plupart bénévoles. Nous ferons tout pour qu'il n'y est pas de couac mais l'erreur est humaine. Dans le cas d'une fermeture nous tacherons de vous informer le plus tôt possible et de vous accompagner dans ce moment.
*   Vous devez respecter la loi.
*   Vous devez faire preuve de civisme et ne pas abuser des services/ressources que nous vous mettons à disposition.

## En long, large et travers

### Condition du service

*   L’utilisation du service se fait à vos propres risques. Le service est fourni tel quel.
*   Le site est accessible, autant que faire se peut, 24 heures sur 24 et 7 jours sur 7\. Alors même que l’association a effectué toutes les démarches nécessaires pour s'assurer de la fiabilité des informations, logiciels et services contenus sur le site, elle ne saurait être tenue responsable d'erreurs, d'omissions, de virus ou des résultats qui pourraient être obtenus ou être la conséquence d'un mauvais usage de ceux-ci.
*   L’association ne saurait être tenue responsable des problèmes et incidents techniques pouvant survenir et entraîner un risque de perte de données. L’association n'est en effet tenue qu'à une simple obligation de moyens.
*   En cas de violation des conditions d'accès et d'utilisation du site, l’association se réserve le droit de suspendre l'accès au site et son utilisation aux visiteurs à tout moment.
*   L'association à sa seule discrétion, a le droit de suspendre ou de résilier votre accès à un service et de refuser toute utilisation actuelle ou future du service.
*   Le visiteur déclare et garantit qu'il connaît parfaitement les risques liés à la navigation Internet.
*   Toute action effectuée par les visiteurs sur un service offert par l'association engage la responsabilité du visiteur. En aucun cas, l’association ne pourra être tenue pour responsable ni des dommages découlant des actions d'un visiteur, ni des dommages que subirait un visiteur.
*   Le visiteur s'engage à ne pas utiliser le service à des fins illégales ou non autorisées.
*   Le visiteur s'engage à ne pas transgresser les lois de son pays.
*   Le visiteur comprend et accepte que l’association ne puisse être tenue responsable pour les contenus publiés sur ce service.
*   Le visiteur s'engage à ne pas transmettre des vers, des virus ou tout autre code de nature malveillante.
*   L'association ne garantit pas que :

*   le service répondra à vos besoins spécifiques,
*   le service sera ininterrompu ou exempte de bugs,
*   que les erreurs dans le service seront corrigés.
*   L’échec de l'association à exercer ou à appliquer tout droit ou disposition des Conditions Générale d’Utilisation ne constitue pas une renonciation à ce droit ou à cette disposition.

### Modification du service

*   L'association se réserve le droit, à tout moment de modifier ou d’interrompre, temporairement ou définitivement, le service avec ou sans préavis.
*   L'association ne sera pas responsable envers vous ou tout tiers pour toute modification, suspension ou interruption du service.

### Droit d’auteur sur le contenu

*   Le visiteur s'engage à ne pas  envoyer, télécharger, publier sur un blog, distribuer, diffuser tout contenu illégal, diffamatoire, harcelant, abusif, frauduleux, contrefait, obscène ou autrement répréhensible.
*   L'association se réserve le droit de supprimer ou d’empêcher la diffusion tout contenu nous paraissant non pertinent pour l’usage du service, selon notre seul jugement.
*   L'association s'engage à ne revendiquer aucun droit sur vos données : textes, images, son, vidéo, ou tout autre élément, que vous téléchargez ou transmettez depuis nos services

### Données personnelles

*   L’association dégage toute responsabilité quant aux contenus produits par le visiteur.
*   Conformément à l’article 34 de la loi « Informatique et Libertés », l'association garantit à l’utilisateur un droit d’opposition, d’accès et de rectification sur les données nominatives le concernant. Le visiteur a la possibilité d’exercer ce droit en contactant l'administrateur du site.
*   Tout comme d’autres services en ligne, l'association enregistre automatiquement certaines informations concernant l'utilisation du service  les données affichées ou cliquées (exemple : liens, éléments de l’interface utilisateur), et d’autres informations pour identifier les visiteurs (exemple : type de navigateur, adresse IP, date et heure de l’accès, URL de référence) :

*   L'association utilise ces informations en interne pour améliorer l’interface utilisateur des services et maintenir une expérience utilisateur cohérente et fiable.
*   Ces données ne sont ni vendues, ni transmises à des tiers.
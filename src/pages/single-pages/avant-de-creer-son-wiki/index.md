---
title: "Avant de créer son wiki"
date: "2018-03-05T17:30:03.284Z"
layout: page
path: "/avant-de-creer-son-wiki/"
---

Un wiki est un site internet avec une édition facile, parfaitement adapté pour réaliser le site intranet collaboratif de son projet. Il permet de documenter des sujets, peut servir d’aide mémoire, et permet la rédaction d’articles a plusieurs. 
[En savoir plus sur les wikis](https://colibris-outilslibres.org/services/les-wikis-sites-collaboratifs-yeswiki/)

<div class="alert alert-info">Avant de créer votre wiki, nous vous demandons de lire cette page et de suivre si possible nos recommandations.</div>

Tout d’abord quelques exemples de wiki au sein de l’écosystème de Colibris :

* le wiki du projet Oasis de Colibris : https://colibris.cc/oasis/
* le wiki du MOOC permaculture : [https://colibris-universite.org/mooc-permaculture](https://colibris-universite.org/mooc-permaculture)
* le wiki de l’Université vivante, liée au Hameau des Buis : [http://www.universite-vivante.org/wiki-univ](http://www.universite-vivante.org/wiki-univ)
* le wiki du réseau des forêts nourricières, animé par GreenFriends : http://foretsnourricieres.greenfriends-europe.org/reseau

<hr />

## Dans quel cas créer son wiki ?

A priori nous vous proposons de créer un wiki si :

* vous participez à un projet collectif (un wiki n’est pas un cloud de stockage de vos documents personnels)
* vous êtes amenés à stocker des informations et/ou à travailler à distance
* vous souhaitez modifier facilement les contenus et collaborer entre membres du projet

<hr />

## Se former avant de commencer

Un wiki a beau être simple d’utilisation, on peut facilement se retrouver bloqué devant son wiki si on n’a pas pris le temps de se former et de connaître les petits trucs et astuces pour coopérer à distance et mettre du contenu sur notre nouveau site.

On vous propose 2 types de formations, à réfléchir en fonction de votre niveau de connaissances préalables. Cela vaut le coup de prendre ce temps si vous voulez vraiment utiliser cet outil. 

1. **Comprenez pourquoi et comment coopérer grâce à un outil numérique**

<div class="row">
  <div class="col photo">
    <img src="./vitaminez.jpg" alt="Parcours vitaminez" />
  </div>
  <div class="col-10">
    <br />
    Pour répondre aux besoins liés à l’usage des wikis, l’Université des colibris a créé un parcours de formation en ligne qui prend environ 2h. C’est le parcours "Vitaminez vos projets collaboratifs à l’aide d’outils numériques".
    <br /><br />
    Nous vous encourageons à le faire, éventuellement à plusieurs, avant pour clarifier vos besoins et découvrir des techniques d’animation d’un outil numérique. Une petite soirée sera suffisante et elle vous fera gagner un temps précieux pour la suite. A vous de jouer !
    <br /><br />
    <a class="btn btn-primary px-5" href="https://colibris-universite.org/formation/parcours-des-outils-libres-pour-vos-projets-collectifs"><i class="fa fa-envelope" aria-hidden="true"></i>
 Faire le parcours</a>
  </div>
</div>
<hr />



2. **Echangez avec les jardiniers de la ferme à wiki**

<div class="row">
  <div class="col photo">
    <img src="./jean-marie-et-sylvere.jpg" alt="jm et sylvere" />
  </div>
  <div class="col-10">
    <br />
    Deux compagnons oasis, Sylvère Janin et Jean-marie Ramel, animent la "ferme à wiki" de Colibris. En bons fermiers, il aident à l’élevage des wikis et vous proposent pour cela des <a href="https://colibris-wiki.org/?WebinairE">webinaires / réunions</a>. Ils vous proposent aussi des rencontres en présentiel pour découvrir yeswiki, en participation consciente.
    <br />
    Pour les projets qui ont envie d’aller vite, les compagnons proposent aussi des formules personnalisés.
    <br /><br />
    <a class="btn btn-primary px-5" href="mailto:ferme@colibris-lemouvement.org"><i class="fa fa-envelope" aria-hidden="true"></i>
 Contacter les jardiniers de la ferme à wiki</a>
  </div>
</div>
<hr />

## Comment créer son wiki ?

Colibris a mis en place une "ferme à wiki" à destination des porteurs de projets qui se reconnaissent dans les valeurs de Colibris. Il permet de créer gratuitement son propre espace collaboratif en YesWiki pour son projet. 

Nous appelons parfois ça une ferme à YesWiki : installer un site soi-même est assez technique, la ferme vous simplifie cette installation. Colibris prend en charge l’hébergement et la maintenance et anime le réseau des wikis sur la ferme.

La création d'un wiki via cette ferme l’installe donc sur un serveur de Colibris. Il y a donc une limite de capacité. Nous reviendrons vers vous par mail pour vérifier que votre espace collaboratif vit toujours : nous supprimerons régulièrement les wikis qui ne vivent pas, pour faire de la place aux nouveaux besoins d'espaces.

Vous pouvez cependant privatiser votre wiki en configurant des mots de passe si vous ne souhaitez pas que d’autres utilisateurs voient vos contenus.

Colibris ne fait bien sûr absolument aucun usage des données sur vos espaces.

<a class="btn btn-primary px-5" href="https://colibris-wiki.org/"><i class="fa fa-hand-o-right" aria-hidden="true"></i>
 Découvrir la ferme à wiki et créer mon wiki</a>
<hr />

## Et après ?

Une fois l’outil créé, vous pouvez commencer par lire les [premières informations techniques pour commencer à modifier votre wiki](https://colibris-wiki.org/?PremiersPas).

Nous vous recommandons aussi d’entrer en contact avec les jardiniers de la ferme qui pourront vous partager différentes propositions. Un accompagnement personnalisé peut être un coup de boost précieux pour faire décoller vos usages collaboratifs.

<a class="btn btn-primary px-5" href="mailto:ferme@colibris-lemouvement.org"><i class="fa fa-envelope" aria-hidden="true"></i> Contacter les jardiniers de la ferme à wiki</a>


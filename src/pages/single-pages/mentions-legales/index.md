---
title: "Mentions légales"
date: "2018-01-29T22:12:03.284Z"
layout: page
path: "/mentions-legales/"
---

## Éditeur

Association Colibris  
Association loi 1901, reconnue d'intérêt général  
N° Siret : 494 678 857 000 33  
n°TVA : FR 21 494 678 857  

### Siège social

Association Colibris  
18-20, rue Euryale Dehaynin  
75019 Paris  

Pour nous contacter : [utilisez notre formulaire de contact](/contact/)  

## Hébergeur

Hetzner Online AG  
Adresse du Centre Administration et du Centre Hébergement:  
Hetzner Online AG  
Stuttgarter St. 1  
91710 Gunzenhausen  
Allemagne  
Tél.: +49 – 9831 - 610061  

## Informatique et libertés

Les informations recueillies sur ce site ou ses sous-domaines font l’objet d’un traitement informatique destinés aux besoins des services proposés par Colibris ou pour des traitements statistiques. En application des articles 39 et suivants de la loi du 6 janvier 1978 modifiée, vous bénéficiez d’un droit d’accès et de rectification aux informations qui vous concernent.  

Si vous souhaitez exercer ce droit et obtenir communication des informations vous concernant, veuillez vous adresser à : contact (at) colibris-outilslibres (point) org  
Les contenus du site colibris-outilslibres.org sont, en dehors des photos, soumis à la licence **Creative Commons BY-SA**  
Pour améliorer l'accès à la culture et à la connaissance libres, Colibris autorise de redistribuer ou réutiliser librement ses contenus pour n'importe quel usage, y compris à des fins commerciales. Une telle utilisation n'est autorisée que si l'auteur est précisé, et la liberté de réutilisation et de redistribution s'applique également à tout travail dérivé de ces contributions.  
Les contenus des sous-domaines *.colibris-outilslibres.org sont publiés sous la responsabilité des utilisateurs.

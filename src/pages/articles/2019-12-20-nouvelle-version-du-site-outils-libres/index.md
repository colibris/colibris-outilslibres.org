---
title: La nouvelle version du site Colibris Outils Libres est arrivée !
date: "2019-12-20T10:00:24.121Z"
layout: post
path: "/nouvelle-version-du-site-outils-libres"
image: ./damier.jpg
type: blog
categories:
  - site
---

_Après plus d'une année et demi de bons et loyaux services, le site Colibris Outils Libres fait sa mue vers une version permettant de faire apparaitre plus de services sur sa page d'accueil._  

## Une page d'accueil affichant plus de services

![damier](./damier-full.jpg)

L'ancienne vue par ligne empêchait de voir l'ensemble des services rapidement, nous avons donc opté pour une version en grille, qui nous permettra d'en rajouter encore en 2020 !

-----

## Trois nouveaux services libres

Certains étaient en ligne depuis un moment, mais jamais bien mis en évidence faut de place sur l'ancien site... Mais les voilà enfin disponibles, avec :

- [les vidéos](https://video.colibris-outilslibres.org) sur le service de diffusion Peertube des Colibris
- [des Post-it](https://postit.colibris-outilslibres.org) collaboratifs pour s'organiser et faire des tempêtes de cerveau, même à distance
- [un raccourcisseur](https://colibris.link) d'Url pour bénéficier de liens courts de la forme colibris.link/\<ce-que-je-veux\>

-----

## Des flux RSS pour recevoir l'information

Dans la partie **Actus**, vous trouverez une icône bien connue des dinosaures de l'Internet :

<div class="text-center">
<a class="rss-link" title="S'abonner au Flux RSS des actus" href="/rss.xml">
<i class="fa fa-rss fa-5x"></i>
</a>
</div>

Ce standard permet de s'abonner aux informations en provenance d'un site sans à avoir à retourner voir le site en permanence.  
Par exemple, le logiciel de courriel [Thunderbird](https://www.thunderbird.net/fr/) permet de s'abonner aux flux RSS et de recevoir nos actualités Outils Libres.
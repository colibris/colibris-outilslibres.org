---
title: Coding Party !
date: "2017-02-07T22:40:32.169Z"
layout: post
type: blog
image: ./codingparty1.jpg
path: "/coding-party/"
categories:
  - événement
  - présentation
---

*Après une expérimentation fructueuse l'année dernière, Colibris s'engage à renouveler l'expérience en proposant durant l'année trois moments de rencontre et de création de logiciels libres ensemble !*

![coding party](./codingparty1.jpg)
_Florestan, Myriam, Sebastian en pleine "Coding party" organisée par Colibris (Habiterre, Drôme, avril 2017)_

## Mais c'est quoi une coding party ?

Alors le concept n'est pas breveté et plusieurs termes existent pour désigner la même chose ([hackathon](https://fr.wikipedia.org/wiki/Hackathon), sprint de développement...).  
L'idée, c'est que des personnes avec des compétences en programmation se retrouvent sur plusieurs jours en immersion et en profitent pour accélerer le développement de certains projets informatiques en commun.  
Dans le milieu associatif, seules les plus grosses structures ont un informaticien et il travaille souvent seul, et dans l'urgence... Le fait de se retrouver permet de s'organiser et de mutualiser, de se former aussi entre nous. 

L'aspect "party" est important aussi : nous sommes dans un processus créatif, sans horaires précis, avec la possibilité de boire des coups, s'amuser entre personnes avec des points communs !

On est conscient qu'en 2 ou 3 jours on ne peut pas tout faire, mais ces rendez-vous réguliers permettent aussi d'organiser le travail à distance par la suite.

## C'est quand ? C'est où ?

Les dates précises ne sont pas encore décidées, mais il y aura une session en avril, une session en été, et une session en octobre/novembre.
Les sessions d'avril et octobre/novembre seront à Paris dans les locaux de Colibris, le weekend.  
Pour le session d'été, nous essaierons de nous mettre au vert ou d'intégrer un événement existant.
Chacune des coding party sera annoncée sur ce site quand l'organisation sera calée, avec un formulaire pour réserver sa place.
En vue de la configuration des lieux, le nombre de participants ne pourra pas dépasser une vingtaine.


## N'importe qui peut venir ? Combien ça coûte ?

C'est ouvert à toutes et à tous, à la condition que vous ayez des compétences en développement. Ce n'est pas un temps de formation, mais un moment de collaboration et donc il faut avoir des connaissances techniques déja acquises.

Pour l'aspect financier, Colibris prend en charge les repas (bio et végétarien) et les boissons, les personnes doivent venir par leur propres moyens et trouver un lieu ou dormir (éventuellement, on aura des pistes !).


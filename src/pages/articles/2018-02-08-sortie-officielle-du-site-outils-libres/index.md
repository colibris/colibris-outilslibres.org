---
title: Sortie officielle du site outils libres
date: "2018-03-28T23:46:37.121Z"
layout: post
path: "/sortie-officielle-du-site-outils-libres/"
image: ./home.jpg
type: blog
categories:
  - présentation
---

_Après une bonne année de développement et de mise en place des services sur nos serveurs, le site Outils libres ouvre ses portes ! Nous vous proposons ici une visite guidée rapide afin de découvrir les principales fonctionnalités du site._

Le site Outils Libres référence des services web basés sur des logiciels libres, gérés et maintenus par l'association Colibris, qui remplacent des services appartenant à des gros groupes (les GAFAMs, pour Google, Amazon, Facebook, Apple et Microsoft) faisant commerce de vos données personnelles.  
L'objectif est de vous permettre de vous organiser, en sachant que nos services respectent votre vie privée, et que le fait que nous utilisions des logiciels libres garantit la transparence (avec un code ouvert) et offre la possibilité (avec un peu de connaissance technique) de vous même installer ces outils sur votre propre serveur et d'avoir un contrôle total sur vos données.  
Pour les moins techniciens, Colibris mets a disposition des services libres et va proposer des tutoriels et des accompagnement à l'utilisation de ces services, afin que la technique ne soit pas une barrière dans la réalisation de vos projets.

Pour en savoir plus sur la raison d'être du projet, vous pouvez consulter la partie [le pourquoi du comment](/le-pourquoi-du-comment/).

## Petite visite guidée
### Vos outils (page d'accueil)
![home](./home-guided.jpg)
La page d'accueil est un accès rapide aux outils : une icône, un bouton d'accès rapide. Une description des outils est aussi disponible sur le lien "En savoir plus" de chaque outil.
En dessous, on retrouve les trois dernières actualités.

-----

### Le pourquoi du comment
Petite explication de la raison pour laquelle Colibris a décidé de mettre en œuvre ce projet.

-----

### Actus
![actus](./actus.jpg)
Les dernières actualités du site, des tutoriels (à venir), les prochains événements, et des articles de veille sur le sujet du libre.

-----

### Des questions ?
Une foire aux questions pour vous donner des éléments de réponse sur le site et ses services, ainsi que sur les concepts de libre et des licences libres.

-----

### Le pied de page
![home](./footer.jpg)
Le pied de page vous permet de :
 - vous inscrire à la lettre des Colibris, pour être informé de l'actualité de tous les projets de l'association
 - voir les sites amis en rapport avec la thématique des outils libres, et le wiki "Espace technique des Colibris" pour les plus techniques d'entre vous
 - voir les autres sites Colibris !!
 - prendre connaissance des infos juridiques, administratives, et techniques sur le site et les services associés

-----

## Des évolutions à venir sur l'année
Nous ne nous interdisons pas de rajouter des services dès que nous aurons le temps de les mettre en place. Sont dans les cartons pour 2018 : un service de formulaire comme [framaforms](https://framaforms.org), un service d'envoi de fichiers volumineux, équivalent de wetransfer mais anonyme et sécurisé, un service de cartographie comme [Près de chez Nous](https://presdecheznous.fr/). Beaucoup de belles choses en perspective !

## Aidez-nous à améliorer le contenu de ce site !
N'hésitez pas à [nous contacter](/contact/) si vous avez des questions, des suggestions d'articles ou des améliorations à proposer!
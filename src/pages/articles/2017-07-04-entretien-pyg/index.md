---
title: "Les logiciels libres : un moyen de passer de la société de la consommation à la société de la contribution"
date: "2018-03-04T22:12:03.284Z"
layout: post
type: blog
image: pyg-noir.jpg
path: "/logiciels-libres-un-moyen-passer-societe-consommation-a-societe-contribution/"
categories:
  - entretien
---

*Entretien avec Pierre-Yves Gosset, par Vincent Tardieu, repris de [Colibris le Mag](https://www.colibris-lemouvement.org/magazine/logiciels-libres-un-moyen-passer-societe-consommation-a-societe-contribution)*

![Pierre-Yves Gosset](./pyg-noir.jpg)

* * *

_Pierre-Yves Gosset est directeur et délégué général de l’association [Framasoft](https://framasoft.org/)_.  

Après un parcours l’ayant amené à travailler en tant qu’ingénieur pédagogique pour plusieurs universités, ainsi qu’au CNRS, il coordonne aujourd’hui les multiples projets de l’association, et notamment la campagne Dégooglisons Internet ainsi que le projet CHATONS. Projet auquel Colibris s’associe pour promouvoir les “outils libres” et les “communs”. Pierre-Yves nous décrypte ici les enjeux de ce combat pour changer le monde du numérique et nos usages. 

* * *

**–  Quelles sont les principales missions de Framasoft ?**

Depuis plus de 10 ans, nous menons des actions d’éducation populaire pour faire découvrir au plus large public non seulement le logiciel libre (Firefox, VLC, Linux, etc.) mais aussi Wikipédia, les biens communs, le droit d’auteur∙e, etc. Et depuis trois ans, nous sommes surtout connus pour notre campagne « [Dégooglisons Internet](https://degooglisons-internet.org) » qui vise à proposer des alternatives libres, éthiques, décentralisées et solidaires aux services de Google, Apple, Facebook, Amazon ou Microsoft (GAFAM). Ainsi, Framasoft propose plus d’une trentaine de services, tels « [Framadate](https://framadate.org/) », une alternative au service « Doodle », qui permet de déterminer une date de rendez-vous entre plusieurs personnes, ou « [Framapad](https://framapad.org/) », qui permet de rédiger des documents de façon collaborative en temps réel. La liste complète peut être trouvée [sur le site](https://degooglisons-internet.org). 

### Triple domination sur nos modes de vie

**– Ce mouvement pour le "libre" et les "communs" est mondial : on sent qu’il y a urgence...**

Absolument, car les enjeux sont essentiels pour nos modes de vie. Les GAFAM exercent en réalité une triple domination. Pas seulement une domination technique avec le déploiement de tas de solutions pour l’informatique et Internet, les objets connectés, les voitures autonomes, la robotique, la santé, l’intelligence artificielle, etc. Une domination économique aussi : ce sont les cinq plus grosses capitalisations boursières mondiales, et Google ou Apple disposent chacune de plus de 100 milliards de dollars de trésorerie, ce qui les rend plus puissantes que certains États. Une domination culturelle enfin, puisqu’elles sont aujourd’hui en train de déployer une vision "californienne" (qu’on pourrait qualifier de protestante, blanche, riche, états-unienne, libérale) de ce que doit être le numérique à l’échelle planétaire, influençant largement notre façon de "faire société" – comme le cinéma américain a pu promouvoir « _l’American Way of Life_ » après guerre, mais cette fois d’une façon plus rapide et beaucoup plus efficace.

Autrement dit, ces multinationales sont en train de modeler un projet de société à l’échelle de la planète, et sans vouloir leur prêter forcément les pires intentions, il convient d’être éveillé et conscients des modèles qu’elles déploient : uberisation, transhumanisme, solutionnisme technologique, intelligence artificielle pour remplacer des emplois, dépendance à quelques acteurs, bulles de filtres, exploitation des données personnelles, surveillance généralisée...

> _“ Les GAFAM exercent une triple domination : technologique, économique, mais aussi culturelle, en influençant largement notre façon de faire société”_

En cela, la cohérence entre les objectifs de Colibris pour une société plus écologique et humaniste, avec des citoyen∙ne∙s plus autonomes, et les outils utilisés par le mouvement, est importante.

**Le Libre en Commun ?**  

Qu’est-ce que les logiciels libres ont en commun avec… les Communs, cette notion – et réalité dans le monde rural au Moyen-Âge – des ressources et biens communs ? En effet, le logiciel libre, et plus largement les projets libres tels que Wikipedia, est bien une ressource immatérielle, mais est-il un commun pour autant ? Et surtout qu'en est-il de sa gestion, de la communauté qui le maintient et l'utilise, des règles que cette communauté a élaborées pour le développer et le partager ?  
Autant de questions clé que nos ami∙e∙s de Makina Corpus, développeur∙se∙s toulousain∙e∙s et expert∙e∙s des logiciels libres, nous proposent d’explorer grâce à cet article, ["Le logiciel libre est-il un commun ?"](https://makina-corpus.com/blog/societe/2017/le-logiciel-libre-est-il-un-commun)

 **– À t’écouter, on se dit qu’au-delà de la diversité des solutions techniques, le "libre" dessine une véritable alternative de société...**

Oui, car sans vouloir paraître trop manichéen, ce monopole des GAFAM pose la question du choix du monde dans lequel nous voulons vivre demain : voulons-nous un monde de l’individualisme, du bien privé, de l’action personnelle, de l’intérêt privé ? Ou préférons-nous un monde du bien commun, de l’action collective, de l’intérêt général ?

Le numérique est aujourd’hui présent partout (dans notre téléphone, dans notre télévision, dans notre voiture, nos livres, notre musiques, notre ville, nos médias...). La question de « _Qui gère ce numérique ?_ » est donc centrale. Aujourd’hui, les États veulent en confier la gestion à des entreprises privées, car il s’agit d’un facteur de forte croissance économique. Le logiciel et la culture libre préfèrent l’offrir, sous forme de "communs", aux citoyen∙ne∙s.

C’est donc en grande partie sur notre capacité à passer de la société de la consommation à la société de la contribution, que la question du libre est relativement primordiale.

**– La dernière loi sur le numérique d’Axelle Lemaire, qui a été saluée par plusieurs acteur∙rice∙s du secteur, ne répondait pas à cette question des licences et de l’accès aux outils ? **

Non. La loi comporte bien entendu quelques avancées notables, et nous ne voulons pas jeter le bébé avec l’eau du bain. Mais concernant le logiciel libre, il y a une vraie réticence du gouvernement précédent – et actuel peut-être, nous verrons – à vouloir privilégier le logiciel libre. En se voulant neutre sur la question, la loi essaie surtout de protéger les intérêts économiques de startups (souvent financées par l’État), dont celles qui réussissent le plus finiront probablement… rachetées par les GAFAM ! 

### Des chatons à l’assaut de la jungle du numérique 

![chatons](./chatons-sabrolasers1.jpg)

**– Vous venez de donner naissance à des drôles de chatons… Peux-tu nous les présenter ?**

Il faut d’abord préciser que Framasoft ne souhaite pas devenir le "Google du libre" : nous nous considérons plutôt comme une AMAP du logiciel libre, sauf qu’au lieu de fournir des choux, des carottes ou des pommes, nous offrons des services en ligne sans publicité, ni collecte de données personnelles. Logiquement, nous avons souhaité monter un "réseau d’AMAP du libre" : c’est l’objet du Collectif des Hébergeurs Transparents, Ouverts, Neutres et Solidaires (CHATONS).

 Aujourd’hui, le collectif compte près d’une trentaine de structures, comme par exemple [Infini](https://infini.fr), une association brestoise qui promeut un usage non-commercial ou coopératif d’Internet ou l’association franc-comtoise [Zaclys](https://zaclys.com) qui propose des services ouverts à tou⋅te⋅s gratuitement ou contre adhésion.

> _“ Nous voulons monter un réseau d’AMAP du libre qui offre des services en ligne coopératifs, sans publicité, ni collecte de données personnelles”_

Et Colibris nous a fait le plaisir de rejoindre ce collectif, en devenant l’un de ces "chatons" ! [Site en ligne très prochainement]

**Où sont les femmes ?**  

Aujourd’hui, seulement 27% des professionnel-le-s du numérique sont des femmes, et 15% des startups sont fondées par des femmes.  
Social Builder est une startup sociale dont la mission est de concrétiser la mixité Femmes-Hommes dans les métiers et sphères de décision, prioritairement dans l’économie numérique. Elle développe le programme _Jeunes Femmes & Numérique_, avec l'objectif d'accélérer les carrières de 100 000 jeunes professionnelles dans le digital d’ici 2025 et rendre le numérique plus mixte.  
Plus d'infos : [jeunesfemmesetnumerique.com](http://jeunesfemmesetnumerique.com/)

**– Les missions de Framasoft et de Colibris peuvent sembler éloignées : en quoi se rencontrent-elles ?**

Sur la question des valeurs, d’abord : mettre l’humain et son intelligence, de préférence collective, au centre de ce mouvement. 

Sur la question de la cohérence ensuite : tout comme l’Assemblée Générale d’une AMAP* ne se ferait pas chez McDonald’s, il paraît incohérent qu’un mouvement humaniste et solidaire utilise les services de Google, par exemple, sans s’interroger sur le modèle de société porté par cette entreprise. Ainsi Colibris peut encore utiliser des outils de Google (_Drive_ par exemple), mais ce mouvement s’est engagé sur une vraie réflexion sur ces outils, en a adopté d’autres (wikis, framapad, framadate…) chaque fois qu’ils sont opérationnels, et prépare une campagne sur les outils libres et les communs. Comme dit le slogan de Framasoft : « _La route est longue, mais la voie est libre !_ »

![coding party](./codingparty1.jpg)
_Florestan, Myriam, Sebastian en pleine _"Coding party"_ organisée par Colibris (Habiterre, Drôme, avril 2017)

**– De très nombreux utilisateurs∙rice∙s d’Internet, des moteurs de recherche et d’applications sur les téléphones portables, s’inquiètent à la fois de la sécurisation de leurs données et du flicage sur celles-ci par divers opérateurs. En quoi des outils libres peuvent-ils répondre à ces inquiétudes et qu’est-ce qui permettrait un avenir numérique heureux ?**

La particularité d’un outil libre est de rendre public et de partager son « code source », c’est à dire sa recette de cuisine. On sait alors de quoi il est composé, comment il fonctionne, quelles sont les actions déclenchées quand on clique sur tel ou tel bouton, etc. Autrement dit, c’est du logiciel sans OGM. Un exemple : lorsque tu regardes une vidéo dans Windows Media Player (Microsoft), ce dernier envoie des informations à Microsoft à ton insu. C'est une modification apportée au code par Microsoft pour mieux suivre l'usage de son logiciel. Sauf que tu n'as peut-être pas envie de faire savoir que tu regardes telle ou telle vidéo. Avec VLC, un logiciel libre, les développeurs de ce logiciel libre s'aperçoivent immédiatement qu’un∙e développeur∙euse intègre un code de traçage malveillant, et supprimeront celui-ci...

### Respect et sécurité : deux valeurs du libre

En conséquence, utiliser des outils libres, c’est utiliser des outils de confiance, qui n’ont aucun intérêt économique à collecter vos données personnelles ou exploiter votre intimité pour orienter vos choix de consommation (ou pour les transmettre à des États en mal d’autorité). 

**– Ces valeurs sont-elles largement répandues parmi les geeks ?**

Mais qui sont les « geeks » ? Je crois qu’ils n’existent pas plus que les « bobos » ! Il s’agit d’une population très hétérogène, composée de nombreuses communautés, qui peuvent avoir des valeurs différentes. Vous pouvez avoir des geeks humanistes, des geeks d’extrême droite, des geeks vegan, etc. Ce que l’on peut dire, en revanche, c’est que les questions de la vie privée et des libertés à l’ère du numérique sont des points d’attention et de vigilance chez la plupart des personnes sensibilisées et intéressées aux questions numériques. Une immense majorité d’entre nous ne souhaitent pas devenir une espèce d’élite oppressive du futur, avec d’un côté celles et ceux qui savent protéger leur intimité numérique, et celles et ceux qui ne le savent pas. Nous souhaitons que le plus grand nombre de personnes possible puissent avoir la même connaissance du numérique, afin de faire leurs choix de façon éclairée. 

* * *

_* Les AMAP sont des Associations pour le Maintien d'une Agriculture Paysanne, destinées à favoriser l'agriculture paysanne et biologique en mettant en lien direct des producteurs et des consommateurs. Ces derniers s'engagent à acheter leur production un prix équitable, en leur apportant une sécurité par des paiements à l’avance._
---
title: "FORMATION EN LIGNE: Créer un projet collectif, méthodes et outils éthiques"
date: "2020-10-23T14:00:00.121Z"
layout: post
path: "/formation-en-ligne-creer-projet-collectif-methodes-et-outils-ethiques"
image: ./img/formation_ucpo_2.jpg
type: blog
categories:
 - formation
 - en ligne
 - numérique éthique
 - projets coopératifs

---

## Un nouveau parcours fait son arrivée au sein de l'Université des Colibris pour les Organisations

Nous utilisons souvent le numérique dans notre travail, mais nous nous trouvons parfois face à des pratiques contraires à nos valeurs : collecte et utilisation de nos données personnelles, publicité, centralisation des outils ...  
Mais heureusement, il existe des alternatives pour s'affranchir des géants du web (Google, Amazon, Facebook, Apple, Microsoft, les fameux GAFAM) : elles sont libres, éthiques, décentralisées, coopératives et solidaires !

![formation](/img/formation_ucpo_2.jpg)

## Le parcours

Dans ce parcours, découvrez les étapes de vie d'un projet à l'aide des outils libres et éthiques.

En cinq modules en ligne, apprenez à conduire un projet collaboratif dans votre travail avec les bons outils.
La formation est composée également de trois webinaires en ligne :

- lundi 2 novembre à 17h : Accueil
- jeudi 19 novembre à 17h : Foire aux questions de mi-parcours
- jeudi 3 décembre à 17h : Clôture

**La participation à l'ensemble du cycle est fortement recommandée.** Une attestation de suivi est délivrée en fin de formation.

## Les + du parcours organisations

- progression ciblée pour les professionnels
- un prise en charge par votre OPCO grâce au Datadock de Colibris
- 3 webinaires inclus
- accompagnement personnalisé et réponse aux questions sur la plateforme
- attestation de suivi

## Session du 2 novembre au 3 décembre

Prix : 250€ HT (300€ TTC) / utilisateur

- l'association Colibris étant référencée dans le Datadock, la formation peut être prise en charge par votre OPCO, et bientôt par votre CPF.
- Tarif réduit possible si vous êtes demandeur d'emploi.

Le nombre de participants étant limité, faites dès à présent une demande de devis/facture (en précisant le nombre de places et vos coordonnées).

<a class="btn btn-lg btn-primary" href="mailto:organisations@colibris-universite.org?subject=Inscription Formation Numérique éthique">Je réserve ma place</a>

## Le contenu

1. Les bases : introduction au principe de projet en coopération et des outils qui seront utilisés durant cette formation

2. Lancer un projet coopératif : déterminer les contours du projet, faire un état des lieux des expériences passées, établir le rôle de chacun dans le projet

3. Travailler à distance : les avantages et inconvénients du travail à distance, la posture du travail à distance en équipe, comment animer une réunion, les outils libres et éthiques pour ce faire

4. Maintenir une dynamique de groupe : suivre les décisions, évaluer le travail réalisé par étape, apprendre à travailler par itérations, maintenir les liens à distance, gérer les tensions et célébrer les réussites

5. Partager ses contenus : principe de licences libres dans le travail, compostabilité du projet, quels outils pour le garder en mémoire et le réutiliser.

**Date limite d'inscription : vendredi 6 novembre à 12h.**

<a class="btn btn-lg btn-primary" href="mailto:organisations@colibris-universite.org?subject=Inscription Formation Numérique éthique">Je réserve ma place</a>
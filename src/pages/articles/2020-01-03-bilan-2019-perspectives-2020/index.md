---
title: Bilan 2019 et perspectives 2020
date: "2020-01-03T10:00:00.121Z"
layout: post
path: "/bilan-2019-perspectives-2020"
image: ./cat-looking-in-crystal-ball.jpg
type: blog
categories:
  - projet
---

_Tout d'abord, nous vous souhaitons une belle année 2020, chères lectrices et chers lecteurs, que votre transition vers une utilisation éthique et libre de l'informatique au service des projets écologiques et coopératifs soit totale et plaisante!_  

Avant de parler des choses à venir pour 2020, petite rétrospective sur le projet Outils libres en 2019.  

## Quelques temps forts de l'année 2019

- Mise en place d'un [service d'identification centralisée](https://monprofil.colibris-lemouvement.org) ([CAS](https://fr.wikipedia.org/wiki/Central_Authentication_Service)) sur les sites colibris, en particulier pour simplifier l'accès aux parcours de l'université lors des Moocs, cela marche aussi pour les [wikis](https://colibris-wiki.org) et le [raccourcisseur l'url](https://colibris.link) mais les 110.000 utilisatrices et utilisateurs de ce service pourront bientôt se connecter à l'ensemble des services Colibris sans avoir à créer de multiples comptes.
- le look Colibris des pads (fait par Sebastian Castro) est officiellement le look par défaut du logiciel libre Etherpad, [lire l'histoire de cette contribution racontée par Pouhiou de l'association Framasoft](https://framablog.org/2019/04/29/des-framapads-plus-beaux-grace-a-une-belle-contribution/)
- 4 superbes webinaires sur les outils libres et la coopération réalisés, à retrouver en accès libre sur la [description du parcours de l'Université des Colibris associé](https://colibris-universite.org/formation/parcours-des-outils-libres-pour-vos-projets-collectifs)
- Participation à une Coding party Yeswiki, avec de grandes avancées sur l'ergonomie, le design du thème de base, et l'éditeur, sortie prévue sur la ferme Colibris prochainement !
- Sortie de nouveaux services et mise à jour du site, [à découvrir sur l'article dédié](/nouvelle-version-du-site-outils-libres)

Merci à l'équipe opérationnelle de Colibris, aux compagnons Oasis Jean-Marie et Sylvère qui animent la ferme à wikis, et aux bénévoles Colibris impliqués, en particulier Jean-Philippe, Sébastien et Sebastian, qui permettent à ce projet d'aller si loin avec des petits moyens pour un projet de technique.

<img src="https://media.giphy.com/media/vyNg5hVvHLzCE/giphy-downsized-large.gif" alt="breaking bad buddies" width="100%" />

-----

## Nos services en quelques chiffres

- [les vidéos](https://video.colibris-outilslibres.org) sont au nombre de 677 vidéos locales, produites par Colibris ou nos partenaires (23279 visionnages de ces vidéos), et notre catalogue fédéré regroupant les vidéos des instances que nous suivons sont au nombre de 3683 vidéos
- 2197 [pads](https://pad.colibris-outilslibres.org) en activité, sachant qu'ils disparaissent automatiquement au bout de 6 mois d'inactivité
- 483 [yeswiki](https://colibris-wiki.org) en fonctionnement pour les projets de la transition
- 1300 [sondages](https://date.colibris-outilslibres.org) pour trouver une date ou pour faire un choix
- 1471 utilisatrices et utilisateurs répartis dans 529 équipes, pour un total de 210588 messages échangés pour le [tchat en équipe](https://tchat.colibris-outilslibres.org)

Des chiffres honorables sans être mirobolants, mais qui nous permettent d'assurer un service de qualité, les serveurs n'étant pas encore saturés !

<img src="https://media.giphy.com/media/a5ptfHj2GqOmk/giphy.gif" alt="serveurs alimentés par un hamster" width="100%" />

-----

## Des choses à améliorer

- A vouloir trop en faire, on ne sort plus rien : l'idée en 2019 était de sortir tous les services en une fois avant la mi-année puis de se concentrer sur la communication et la documentation, mais il fallu attendre la fin de l'année pour que tout soit prêt, alors que certains services attendaient juste de sortir...  
La maxime du logiciel libre **"Publier rapidement, publier souvent"** sera de rigueur pour l'année à venir, avec une sortie des outils dès qu'ils sont prêts et donc mieux répartis sur l'année
- Modèle économique à trouver, du moins diversifier les sources de financement : pour l'instant les dons à l'association Colibris couvrent les frais du projet, mais nous réfléchissons dans un premier temps à proposer des formations payantes pour ramener un peu de sous, tout en diffusant le projet et en augmentant les compétences dans notre réseau, à voir si la mayonnaise prendra en 2020
- Manque de temps pour le collectif des [Chatons](https://chatons.org) et autres réseautages, malgré la participation aux votes et aux réunions mensuelles, il faudrait aller plus loin dans l'essaimage du projet, et faire plus de liens avec d'autres structures aux idéaux proches. En espérant que ce temps là sera disponible.
- Peu de temps pour écrire des articles et des tutoriels, si des bénévoles souhaitent s'emparer du sujet, c'est le bon moment!

<img src="https://media.giphy.com/media/oGzFZek2lszlK/giphy.gif" alt="big bang theory, need help" width="100%" />

-----

## Le projet Outils Libres pour 2020

### Sortie de nouveaux services

_Attention, certains liens sont déjà disponibles mais ne sont là que pour se faire une idée et ne doivent pas être utilisés officiellement encore._

- [Framadrop](https://framadrop.org) : une alternative à WeTransfer pour partager de gros fichiers, qui devait sortir en 2019 mais nécessite encore un peu de travail graphique...
- [Photothèque Colibris](https://photos.colibris-outilslibres.org) : proposer une bibliothèque d'images jolies et dans les thématiques colibris, sous licence Creative Commons.
- [Gogocarto](https://gogocarto.fr) : un service de ferme de cartes participatives, pour réaliser l'équivalent de la carte [Près de chez nous](https://presdecheznous.fr/) pour vos projets et thématiques.
- [Mobilizon](https://joinmobilizon.org), si nous arrivons à brancher notre identification centralisée dessus, nous sortirons notre instance en même temps que Framasoft. Mobilizon est une alternative décentralisée aux évènements Facebook et services d'inscription à des évènements comme Meetup

### Participation à des évènements et codings party

- 30 janvier 2020 : Participation à une table ronde sur le sujet "Numérique et Responsabilité des associations" lors d'[une journée organisée par Webassoc](https://www.webassoc.org/events/waday2020/)
- 1 au 3 février : participation au [FOSDEM](https://fosdem.org/2020/), le plus gros salon européen autour du logiciel libre, regroupant 8000 hackers
- Sans doute en mars, sur Paris, Coding party d'amélioration de gogocarto, avant sa sortie officielle probable
- la semaine du 27 avril au 2 mai, au Vigan, en partenariat avec [R d'Evolution](http://www.rdevolution.org/) pour une double session :  
  - pour expérimenter autour du Raspberry Pi comme serveur d'outils libres
  - pour faire une coding party avec les membres de l'association YesWiki

Voila déjà un premier semestre bien rempli, pour la suite, laissons de la place à l'imprévu!

### Améliorations pour la ferme à wiki

Une fois la nouvelle version de YesWiki sortie, notre ferme mettra à jour l'ensemble des sites afin de proposer de nouveaux thèmes, et de nouvelles fonctionnalités à toutes et tous.  
Pour vous donner envie, sachez qu'en plus de nouvelles extensions permettrons de faire des quiz et de sortir plus facilement les données vers votre hébergement à vous, ou vers une sortie au format PDF pour ne plus consommer d'énergie en ligne inutilement!  

Enfin, nous allons proposer des formations aux logiciels et services libres, avec une utilisation centrale de YesWiki, si vous êtes intéressés pour des formations sur mesure, merci de prendre contact avec nous!

-----

Voilà, merci de votre intérêt pour le projet, et n'hésitez pas [à nous contacter](http://colibris-outilslibres.org/contact), pour nous faire part de choses à améliorer, pour toute question, toute proposition de coup de main, soutien financier, ou autre !

<img src="https://media.giphy.com/media/jRlP4zbERYW5HoCLvX/giphy.gif" alt="gif de chat qui fait des bisous" width="100%" />
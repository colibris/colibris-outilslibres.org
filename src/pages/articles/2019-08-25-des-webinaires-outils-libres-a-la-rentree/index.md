---
title: Des webinaires outils libres à la rentrée !
date: "2019-08-25T15:40:24.121Z"
layout: post
path: "/des-webinaires-outils-libres-a-la-rentree/"
image: ./geeks.jpg
type: blog
categories:
  - webinaires
---

_Mesurez-vous à quel point les acteurs du web façonnent nos manières de penser et d'interagir ? Savez-vous expliquer pourquoi les outils libres et décentralisés sont une alternative essentielle face à l'hyper-centralisation du savoir et des outils prônée par les GAFAM ?_  

Pour répondre à ces questions, nous vous avons concoté 4 webinaires hebdomadaires d'une heure avec des intervenants spécialistes de la coopération.

**Ces webinaires sont gratuits et ouverts à tous !**

# - AU PROGRAMME -

## Quelles postures et quelles compétences pour coopérer ?

<div class="row">
  <div class="col photo">
    <img src="./webinaire1.jpg" alt="webinaire1" />
  </div>
  <div class="col-9">
    <h3>Mercredi 4 septembre 20h30</h3>
    Avec <a href="http://cread.espe-bretagne.fr/membres/esanojca">Elzbieta Sanojca</a>, docteure à l'université de Rennes, auteure d'une thèse sur les compétences collaboratives
et <a href="https://cocotier.xyz/?PagePrincipale">Laurent Marseault</a>, spécialiste du fonctionnement en réseaux et secoueur de cocotier notoire.
  </div>
</div>

*****

## Coopérer : mode d'emploi

<div class="row">
  <div class="col photo">
    <img src="./webinaire2.jpg" alt="webinaire2" />
  </div>
  <div class="col-9">
    <h3>Mardi 10 septembre 20h30</h3>
    Retour d'expérience de <a href="http://www.cooperations.infini.fr/spip.php?article10920">Louise Didier</a>, formatrice Animacoop et accompagnatrice de projets collaboratifs (collectif Garcess) et <a href="https://cooptic.be/wakka.php?wiki=QuI">Gatien Bataille</a>, organisateur des rencontres co-construire et accompagnateur de collectifs en Belgique (cooptic).
  </div>
</div>

*****

## Les outils libres et les wikis en pratique

<div class="row">
  <div class="col photo">
    <img src="./webinaire3.jpg" alt="webinaire3" />
  </div>
  <div class="col-9">
    <h3>Mercredi 18 septembre 20h30</h3>
    Découvrez un panel d'outils libres et apprenez comment bien démarrer avec son wiki grâce à des exemples concrets présentés par <a href="https://colibris-wiki.org/?CestQui">Sylvère Janin</a>, jardinier en chef de la Ferme à wikis Colibris, et <a href="https://www.colibris-lemouvement.org/node/702">Florian Schmitt</a>, responsable du projet Outils Libres.
  </div>
</div>

*****

## Le libre peut il sauver le monde ?

<div class="row">
  <div class="col photo">
    <img src="./webinaire4.jpg" alt="webinaire4" />
  </div>
  <div class="col-9">
    <h3>Mercredi 25 septembre 20h30</h3>
    Conférence exceptionnelle de clôture, avec <a href="https://fr.wikipedia.org/wiki/Tristan_Nitot">Tristan Nitot</a>, vice-président de Qwant, <a href="https://forum.chatons.org/t/angie-nouvelle-salariee-chez-framasoft/279">Angie Gaudion</a> chargée de communication de <a href="https://framasoft.org">Framasoft</a> et coordinatrice du collectif Chatons, et des surprises...
  </div>
</div>

*****

👉 [Inscrivez-vous au parcours "Des outils libres pour vos projets collectifs"](https://colibris-universite.org/formation/parcours-des-outils-libres-pour-vos-projets-collectifs), et vous recevrez une invitation avant chaque webinaire !

*****

PS : la plateforme de mooc et la plateforme de webinaire ne sont pas des logiciels libres, par faute de temps et de moyens nous passerons donc par là cette fois ci, mais nous réflechissons à des solutions plus cohérentes sur le long terme, merci de votre compréhension

---
title: "Trouver des médias sous licence libre"
date: "2020-11-30T14:00:00.121Z"
layout: post
path: "/trouver-medias-sous-licence-libre"
image: ./img/vnwayne-fan-Zqmia99hgF8-unsplash.jpg
type: blog
categories:
 - formation
 - en ligne
 - numérique éthique
 - projets coopératifs

---

*Retrouvez une liste de liens pour récupérer tous type de médias libres de droits, issue d'une activité du mooc Numérique Éthique de l'université des Colibris pour les organisations.*

![bibliotheque](img/vnwayne-fan-Zqmia99hgF8-unsplash.jpg)

## Les images libres

- [https://unsplash.com/](https://unsplash.com/) : de belles photos professionnelles en haute définition libres de droits
- [https://pixabay.com/fr/](https://pixabay.com/fr/) : plus de 1.8 million d'images gratuites et de haute qualité
- [https://commons.wikimedia.org/wiki/Accueil](https://commons.wikimedia.org/wiki/Accueil) : une médiathèque d'images, de sons et d'autres médias audio-visuels sous licence libre (alimentant wikipedia)

## Les musiques libres

- [https://www.dogmazic.net/](https://www.dogmazic.net/) : propose plus de 55 000 titres musicaux, tous téléchargeables gratuitement "en toute quiétude et en toute légalité"
- <http://www.ziklibrenbib.fr> : de la curation de musiques libres par un réseau de médiathèque
- [https://freemusicarchive.org/](https://freemusicarchive.org/) : la référence anglophone en matière de musique libre
- [Opsound](http://opsound.org/) , [CCMixer](http://ccmixter.org/find-music) , [SoundCloud](http://soundcloud.com/creativecommons) , [Bandcamp](https://bandcamp.com/tag/creative-commons) (en anglais aussi, et les 2 dernières plateformes acceptent les Creative Commons, mais toute la musique hébergée n’y est pas libre).

## Les vidéos libres

- [https://sepiasearch.org/](https://sepiasearch.org/) : pour rechercher des vidéos sur les instances peertube (alternative à youtube), toutes les vidéos ne sont pas forcément libres, toutefois.
- https://pixabay.com/fr/videos/ : vidéos pixabay

## Les livres libres

- https://fr.wikisource.org/wiki/Wikisource:Accueil : une bibliothèque de plus de 300.000 textes libres et gratuits
- https://framabook.org/ : un projet éditorial de l'association Framasoft inscrit dans la culture des biens communs. Il propose un modèle économique basé sur les licences libres et favorise la diffusion des contenus.
- https://ebookenbib.net/category/packs/ : des packs de livres électroniques libres par thématique

## Autres contenus libres

- les partages de connaissances : [l'encyclopédie Wikipedia](https://fr.wikipedia.org/) , [Open Street Map](https://www.openstreetmap.org), ...
- les contenus de cours : [Animacoop](https://source.animacoop.net) , [Ritimo](https://www.ritimo.org/Les-dossiers-de-l-ECSI) , [Cooptic](https://cooptic.be/?RessourceS) , les [moocs et parcours Colibris](https://colibris-universite.org), ...
- les [objets à fabriquer soit même](https://www.instructables.com/projects/) , [les plans pour des outils agricoles](https://latelierpaysan.org/Plans-et-Tutoriels) , [les yahourts!](http://nomad-yo.org/wakka.php?wiki=LaRecette)
- liste loin d'être exhaustive !

## En savoir plus

- [https://source.animacoop.net/?Module8TrouverMediasLibres](https://source.animacoop.net/?Module8TrouverMediasLibres) : le module complet d'Animacoop sur "Trouver des médias sous licences libres"
- [https://framacolibri.org/t/musique-libre-vos-coups-de-coeur/2177](https://framacolibri.org/t/musique-libre-vos-coups-de-coeur/2177) : une page de forum Framasoft pour partager des liens de musiques libres

------

Crédit photo : [vnwayne fan](https://unsplash.com/@vnwayne?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText) sur [Unsplash](https://unsplash.com/s/photos/library?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText)
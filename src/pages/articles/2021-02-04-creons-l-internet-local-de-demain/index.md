---
title: "Créons l'internet local de demain !"
date: "2021-02-04T14:00:00.121Z"
layout: post
path: "/creons-l-internet-local-de-demain"
image: ./img/raspberry-pi-solar.jpg
type: blog
categories:
 - résilience
 - numérique éthique
 - circuits courts

---

![image](./img/igromania-1894847_1920.jpg)

**_"Internet est le réseau informatique mondial accessible au public."_ (Wikipedia). Et si ce doux rêve n'avait jamais vraiment existé ? Depuis des années les entreprises de la tech, comme Amazon, ne cessent d'accroître leur contrôle de nos réseaux, créant de gigantesques nœuds où toutes nos opérations et communications passent. À Colibris, nous sommes convaincus qu'un internet sobre et solidaire doit retrouver son idée d'origine : un échange libre et simple de communication. Depuis 5 ans, notre [projet Outils Libres](https://www.colibris-outilslibres.org/) soutient les milliers de personnes qui pensent et développent un internet plus sobre, décentralisé et sécurisé. Découvrez ses projets phares !**

## Nos contrib'ateliers et coding-party

Ce sont des moments précieux où des développeurs et des passionnés du numérique se retrouvent pour développer ensemble de nouveaux outils libres résilients, de nouveaux programmes, faire de la documentation, tester, se co-former... Durant le Festival Oasis 2020, de vaillants hackers se sont rejoints :

<iframe sandbox="allow-same-origin allow-scripts allow-popups" src="https://video.colibris-outilslibres.org/videos/embed/c099b2d7-c743-4751-8851-3dcf1314344d" allowfullscreen scrolling="no" width="689" height="387" frameborder="0">
</iframe>

Nous développons des outils pour vous et vous facilitons l'accès à d'autres.  En décembre, les logiciels Google sont tombés pendant 30 min, nous révélant l'ampleur de notre dépendance à leurs outils. [Il est temps de changer de cap !](https://www.colibris-lemouvement.org/magazine/google-est-mortel-longue-vie-aux-outils-libres)

## Nos projets 2021

### Le Raspberry pi à l'honneur

En 2021 nous accompagnerons [des Oasis](https://cooperative-oasis.org/les-oasis/) et collectifs pilotes pour avoir des services libres locaux, à base de[ Raspberry Pi,](https://www.colibris-lemouvement.org/magazine/mini-ordinateurs-pour-un-internet-plus-sobre-et-plus-ethique) dans leur mise en cohésion de leurs outils et leurs valeurs, avec l’idée de faire des points d'avancement réguliers, un suivi personnalisé, et de documenter la démarche pour qu'elle puisse être répliquée facilement. Des formations Raspberry Pi avec [Ritimo](https://www.ritimo.org/) seront mises en place pour que vous puissiez prendre en main votre nouveau petit ordinateur.

### Plus de tutos

Les services existants seront améliorés, mis à jour et déplacés sur des machines plus puissantes pour tenir une charge de plus en plus importante. Nous souhaitons continuer de faire des tutoriels vidéos et mieux expliquer comment utiliser nos services :

<iframe sandbox="allow-same-origin allow-scripts allow-popups" src="https://video.colibris-outilslibres.org/videos/embed/a41b33e5-f424-480c-a92d-d4089c058c79" allowfullscreen scrolling="no" width="689" height="387" frameborder="0">
</iframe>

### Formation et ateliers

De nouveaux contrib’ateliers seront organisés, avec de beaux moments d’échanges et de développements, et nous continuerons de proposer des [formations Numérique éthique](https://colibris-universite.org/formation/creer-un-projet-collectif-methodes-et-outils-ethiques) avec l'Université des colibris !

Pour participer ou en savoir plus sur ces projets, [contactez-nous !](https://www.colibris-outilslibres.org/contact/)

![participants contribatelier](./img/img_20201004_115519.jpg)

## Les objectifs du mouvement libriste

Le projet Outils Libres s'ancre dans un réseau plus large que l'on nomme communément "[le mouvement du logiciel libre](https://fr.wikipedia.org/wiki/Mouvement_du_logiciel_libre)", qui désigne des logiciels qui respectent la liberté des utilisateurs. En gros, cela veut dire que les utilisateurs ont la liberté d'utiliser, d'exécuter, copier, distribuer, étudier, modifier et améliorer ces logiciels.

Mais souvent, le libre va bien au-delà du code des logiciels, puisqu'on peut aussi partager sous licence libre d'autres types de contenus : images, vidéos, cours en ligne... comme présenté dans notre article "[Trouver des médias sous licence libre](https://colibris-outilslibres.org/trouver-medias-sous-licence-libre)".

![image](./img/framasoft.png)

Enfin, les enjeux du libre se situent maintenant sur un internet libre, non contrôlé par des grosses entreprises, et accessible à tous. Pour cela, dans l'internet francophone, s'est monté le réseau des [CHATONS](https://chatons.org/) qui vise à rassembler des structures proposant des services en ligne libres, éthiques et décentralisés afin de permettre aux utilisateurs et aux utilisatrices de trouver rapidement des alternatives, respectueuses de leurs données et de leur vie privée, aux services proposés par les GAFAM (Google, Apple, Facebook, Amazon, Microsoft).

----

Crédits photos :  

- <https://howchoo.com/g/mmfkn2rhoth/raspberry-pi-solar-power> pour la vignette
- [©Framasoft](https://framasoft.org/) pour la carte des services

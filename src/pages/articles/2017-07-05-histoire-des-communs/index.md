---
title: L’histoire méconnue des communs
date: "2017-07-05T22:40:32.169Z"
layout: post
type: blog
path: "/histoire-meconnue-des-communs/"
image: stone-wall.jpg
categories:
  - Communs
---

*Article d'Anne Lechêne, 5 juillet 2017, repris de [Colibris le Mag](https://colibris-lemouvement.org/magazine/lhistoire-meconnue-communs)*

![](./stone-wall.jpg)

* * *

_Voici une histoire vraie, qui commence au Moyen-Âge et est directement reliée à l’essor d’Internet et du logiciel libre : l’histoire méconnue des communs. Elle fut exhumée en 1990 par Elinor Ostrom des poubelles de l’Histoire où l’avait projetée un certain biologiste Garrett Hardin par un article de 1968 : _« The Tragedy of The Commons »_.

* * *

### Histoire et contre-histoire du mouvement des Enclosures

L’histoire se passe au Moyen-Âge, dans les campagnes d’Angleterre, où les paysan∙ne∙s avaient depuis des temps immémoriaux le droit de couper du bois, des genêts, de récolter du miel ou de mener paître leurs animaux sur des terrains communaux. Ces terrains – landes, forêts, garrigues et autres prés communaux – gérés en commun, n’étaient pas délimités par des clôtures et n’appartenaient à personne en particulier. 

Les paysan∙ne∙s usaient ainsi d’un droit coutumier d’usage sur ces biens communaux, sans avoir à payer une contrepartie à la communauté ou au seigneur, contrairement à d’autres droits seigneuriaux comme l’usage du four à pain ou du moulin. Avec le droit de pacage et le droit de glanage, le droit d’usage des communaux procurait une certaine sécurité aux familles paysannes, pour leurs besoins fondamentaux de la vie courante.

Cette organisation traditionnelle fut remise en question à partir du 12e siècle, et surtout entre le 15e et le 18e siècle, avec le développement en Angleterre de la production et du commerce mondial de la laine. Les riches propriétaires foncier∙ère∙s cherchèrent à agrandir les surfaces de pâturages nécessaires à l’élevage des moutons pour la laine. Les terrains communaux furent clôturés et rendus aux moutons, tandis que les familles paysannes tombaient dans la précarité. C’est ce que les historien∙ne∙s ont appelé le mouvement des Enclosures. Au fil des 17e et 18e siècles, la Chambre des Communes, le Parlement anglais, mettait fin aux droits d’usage et démantelait les Communaux par les _Enclosure Acts_.

C’est tout un « sens de l’Histoire » favorable au libéralisme économique qui est mis en scène, avec ce premier acte d’enclosure de ressources naturelles, précédemment gérées au bénéfice de la communauté de façon coutumière. Une contre-histoire de ces évènements coexiste cependant. Dès 1516 Thomas More écrivait dans _Utopia_ : _« Vos moutons, que vous dites d'un naturel doux et d'un tempérament docile, dévorent pourtant les hommes … »_. Mais durant plusieurs siècles, cette autre petite musique ne fera pas grand bruit.

![](./moutons-980.jpg)
_Crédits : VServat_

### The Tragedy of the Commons

Les moutons s’invitent à nouveau dans la littérature sur les communs, comme personnages d’une fable philosophico-économique de Garret Hardin, dans un célèbre article paru dans la revue Science en 1968, intitulé _« The Tragedy of The Commons »_.

Dans cet article, Garrett Hardin pense avoir démontré que l’humanité est incapable de gérer un bien comme un commun. Prenant l’exemple d’un pâturage laissé en libre accès, il affirme que des berger∙ère∙s cherchant à maximiser leur bénéfice suivent toujours leur intérêt - qui est d’amener paître davantage de moutons sur le champ. Au final, l’exploitation en commun de la ressource ne peut selon lui conduire qu’à sa destruction. Et l’auteur de conclure que seule l’appropriation, qu’elle soit le fait du marché (propriété privée) ou de l’État (propriété publique), peut garantir à long terme la préservation des ressources. C'est ce que Garrett Hardin nomme « la tragédie des communs », qui permettrait ainsi d’expliquer les problèmes de pollution et l'épuisement des ressources naturelles communes. 

L’article devient rapidement une référence tant pour les économistes que pour les écologistes, contribuant à justifier dans la deuxième partie du 20e siècle la gestion directe soit par le marché, soit par les États, de l’eau, de la mer, des forêts ou des espaces naturels des peuples indigènes. Mais l’article relance aussi l’intérêt des chercheur∙se∙s sur le sujet de la gestion des ressources naturelles et du risque de leur épuisement. Et arrive le moment où la théorie de Hardin est remise en question.

![](./propriete-privee.jpg)
_Crédits : Audesou. CC-BY-NC-ND. Source : Flickr_

### Elinor Ostrom et les 8 principes de gouvernance des communs

Elinor Ostrom, économiste et politologue américaine, reprend à la base la question de la gestion des biens communs avec des observations empiriques. Ses études de terrain, menées sur plusieurs continents, lui permettent de constater que des communautés humaines sont capables de gérer des ressources communes telles que des pêcheries, des systèmes d’irrigation, des nappes phréatiques, des forêts ou des pâturages, de façon plus efficace pour l’exploitation comme pour la préservation à long terme de la ressource. 

Dans son ouvrage _Governing The Commons_, publié en 1990, Elinor Ostrom met en évidence un ensemble de principes à respecter par la communauté pour y parvenir. La liste de ces 8 critères est aujourd’hui aussi célèbre que l’avait été l’article de Hardin. Ils définissent les conditions de mise en place d’une gouvernance ouverte : 

— des groupes aux frontières définies ;

— des règles régissant l’usage des biens collectifs qui répondent aux spécificités et besoins locaux ;

— la capacité des individus concernés à les modifier ;

— le respect de ces règles par les autorités extérieures ;

— le contrôle du respect des règles par la communauté qui dispose d’un système de sanctions graduées ;

— l’accès à des mécanismes de résolution des conflits peu coûteux ;

— la résolution des conflits et activités de gouvernance organisées en strates différentes et imbriquées.

À l’opposé des théories abstraites et uniformes sur le comportement d’_homo œconomicus_, les 8 principes d’Elinor Ostrom mettent en valeur la créativité et la résilience des groupes humains pour se doter de systèmes de gouvernance de leurs biens communs. Une bonne nouvelle pour la planète et ses ressources que l’on découvre limitées, et déjà fortement surexploitées, à la même période. 

### Les communs de la connaissance

La connaissance est souvent considérée par les économistes comme un bien public au sens de Paul Samuelson, c’est-à-dire non-excluable (il est difficile d’empêcher le savoir de circuler) et non-rival (ce que je sais ne prive personne du même savoir). Pourtant, des phénomènes d’enclosure peuvent se produire dans la diffusion de la connaissance, et c’est précisément ce qui intéresse le juriste américain James Boyle. Dans son ouvrage _The Public Domain : Enclosing The Commons Of The Mind_, paru en 2003, James Boyle compare l’extension continue des droits de propriété intellectuelle à un « _second mouvement __d’enclosure_ », menaçant l’accès à la connaissance conçue comme un bien commun.

Dans la suite de ses travaux, Elinor Ostrom s’intéresse à son tour aux communs de la connaissance. L’ouvrage collectif qu’elle codirige avec sa collègue Charlotte Hess, sur les nouveaux communs de la connaissance, qui paraît en 2007, propose d’ailleurs une définition des communs plus englobante : « _les communs sont des ressources partagées par un groupe de personnes et qui sont vulnérables aux dégradations et aux __enclosures_ ».

Dans cette nouvelle approche, les communs ne recouvrent plus une simple catégorie de biens, comme les biens naturels, mais des agencements de rapports sociaux qui contribuent à leur production ou leur maintien, des systèmes de règles sociales et de gouvernance pour des actions collectives.

Les travaux d’Elinor Ostrom seront couronnés par le prix Nobel d’économie en 2009, ce qui mettra un nouveau coup de projecteur sur le sujet des biens communs et sur son approche intellectuelle, la « _nouvelle économie institutionnelle_ » qui interroge le rôle joué par les institutions dans la coordination économique.

### Extension numérique du domaine des communs

Avec le réseau Internet, le mouvement des communs rencontre un nouvel objet à intégrer à son nouveau cadre théorique. Réseau distribué, dont les protocoles, les règles et les normes sont discutés par un collectif d’ingénieur∙e∙s qui sont aussi producteur∙rice∙s du réseau, Internet est rapidement reconnu comme un commun. La question de la neutralité du net, un combat très en vue aujourd’hui, pose clairement la réalité du risque d’enclosure, tout comme l’apparition en deux décennies des firmes géantes du Net.![](./creative-commons-980-grey.jpg)

La communauté du logiciel libre ou _open source_, face aux géants de l’informatique et des logiciels propriétaires, promeut et illustre avec éclat qu’il est possible de créer et de gérer des communs numériques de façon efficiente. Choisir d’ouvrir le code à qui veut, pour le modifier ou l’améliorer, se révèle une idée d’une puissance fantastique, pour mobiliser la créativité d’une communauté de développeur∙se∙s rassemblé∙e∙s autour de la vision partagée d’une liberté à défendre. Ces expériences de collaboration à très grande échelle et à distance, avec des machines en réseau, changent l’horizon de l’organisation du travail collaboratif humain et deviennent une source d’inspiration pour d’autres secteurs d’activité.

Les transformations de l’économie de la connaissance, sous l’effet de la digitalisation, ont donné lieu à des concrétisations inspirées par la culture du libre et des communs : ainsi le mouvement de l’_Open Access_ ou libre accès, pour les publications universitaires et l’approche des licences libres ([_Creative Commons_](http://creativecommons.fr/)) face au droit d’auteur∙e et au copyright. 

Les années 2010 voient le triomphe des plateformes (Facebook, Amazon, Uber ou AirBnB) dont le modèle économique « _winner takes all_ » pousse à grande vitesse à l’apparition d’acteur∙rice∙s dominant∙e∙s qui captent la création de valeur. Il n’existe pas de loi anti-trust dans le droit international pour réguler la chose. Mais la culture du libre et des communs s’intéresse aussi aux économies de plateformes, en repensant les modes de gouvernance et en substituant à la logique de plateforme une logique de réseau collaboratif. Ainsi des initiatives comme [Framasoft](https://framasoft.org/) ou [Outils-Réseaux](http://outils-reseaux.org/PagePrincipale), qui visent à renforcer l’autonomie des individus face aux grands acteurs oligopolistiques, avec des outils créatifs et innovants comme les wikis* ou les chatons**.

C’est donc un foisonnement d’initiatives, de différentes communautés d’acteur∙rice∙s du champ de la connaissance et des réseaux qui poursuit l’extension du domaine des communs aux services numériques. 

Très récemment en France, le Conseil National du Numérique (CNNum) a remis un rapport « Ambition numérique » au Premier Ministre, dans le cadre de la préparation de la loi de 2016 pour une République numérique. Il recommande de « _promouvoir le développement des communs dans la société_ ». Une nouvelle petite musique s’élève aux portes de la puissance publique…

Des années 1990 aux années 2010, le renouveau de la théorie des communs sous l’impulsion d’Elinor Ostrom, couplé à l’émergence de phénomènes planétaires majeurs – un début de prise de conscience de la destruction des ressources naturelles, et la montée en puissance de communs numériques aux côtés d’acteur∙rice∙s surpuissant∙e∙s, ont mis ces questions sur le devant de la scène.

La malédiction des communs est bel et bien levée. Il était temps… Ainsi se découvrent une autre histoire à écrire, nécessairement en commun, et d’autres chemins sous nos pieds, pour protéger et gérer les ressources naturelles et immatérielles que nous avons et créons en commun. 

## POUR ALLER + LOIN

- Article [Elinor Ostrom](https://fr.wikipedia.org/wiki/Elinor_Ostrom) sur Wikipedia 

- CNNum. [_Rapport Ambition numérique : Pour une politique française et européenne de la transition numérique_](https://contribuez.cnnumerique.fr/), juin 2015.

- Hardin, Garrett. [_La Tragédie des Communaux_](http://lanredec.free.fr/polis/art_tragedy_of_the_commons_tr.html) (traduction de l’article _The Tragedy of The Commons_, publié en 1968 dans la revue Science par Garrett Hardin). Disponible en ligne.

- Maurel, Lionel. _Comprendre les risques __d’enclosure__ des communs de la connaissance : Réponse à Allan Greer_. La vie des idées, La Vie des Idées, 2015.

Cet article est publié sous la licence [Creative Commons By-SA](https://www.colibris-lemouvement.org/mentions-legales-credits#CC), tout comme l'intégralité de ce site, à l'exception des photos. 

* * *

_* Les wikis sont des sites web collaboratifs dont le contenu peut être modifié par les internautes autorisés._

_** CHATONS : Collectif des Hébergeurs Transparents, Ouverts, Neutres et Solidaires. Voir [l'entretien avec Pierre-Yves Gosset](/logiciels-libres-un-moyen-passer-societe-consommation-a-societe-contribution) pour en savoir plus._
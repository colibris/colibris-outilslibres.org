---
title: "Changer de fournisseur de courriel : êtes-vous prêt⋅e à vous faire la mail ?"
date: "2022-06-16T14:00:00.121Z"
layout: post
path: "/changer-de-fournisseur-de-courrier-electronique-etes-vous-pret-a-vous-faire-la-mail"
image: ./img/retrodeliverypigeon.png
type: blog
categories:
 - email
 - numérique éthique
 - GAFAM
---

*Depuis maintenant deux mois, utilisateur⋅ice⋅s d’adresses @colibris-lemouvement.org, nous rencontrons des difficultés importantes pour envoyer des courriels à des personnes possédant une adresse mail “gmail”.
Nos mails reviennent vers nous avec un message, un peu brut, disant tout simplement qu’ils n’ont pas été délivrés car considérés comme du spam, rendant ainsi nos échanges impossibles…*
## Que se cache t-il derrière le blocage de nos envois ?

Pourquoi nos mails sont rejetés ? Pourquoi sont-ils considérés comme du courrier non sollicité, non désiré ? Difficile à croire quand cela arrive lorsque nous essayons de répondre à nos correspondants / à des messages qui nous sollicitent… Quelle en est la raison ? AUCUNE IDÉE !  

Nos paramètres de configuration paraissent bons. Notre fournisseur de courriels nous explique que tout est bien configuré. Et bien entendu, nos appels au support de Google restent lettres mortes...  

Mais, tiens, pourquoi parle-t-on de Google ? Car gmail est un produit de Google. Et vous pensez bien que chez Google, ils ont d’autres choses à faire que répondre aux colibris… Et pendant ce temps, nos correspondant⋅es attendent leurs réponses.   

Une solution qui fonctionne et qui nous a été soufflé : « quittez votre hébergeur d’emails et choisissez une solution qui fonctionne à coup sûr : celle de Microsoft (appelée Outlook.com), de Google (gmail.com, vous aurez suivi) par exemple ». C’est la solution la plus simple : quitter vos petits fournisseurs de courriels habituels pour migrer chez les «&nbsp;gros&nbsp;».  

Et bien non, à Colibris, notre éthique nous empêche de suivre cette proposition et de jouer le jeu des GAFAM (Google, Apple, Facebook, Amazon et Microsoft)&nbsp;!  

Pourquoi ? Tentons d'expliquer concrètement les conséquences de laisser les GAFAM gérer nos/vos mails.  
En effet, de par leur positionnement central et incontournable, ce sont ces acteurs qui dictent les règles (aussi dans le monde du numérique), qui font la pluie et le beau temps et qui, en rendant la vie impossible aux autres fournisseurs plus petits et plus éthiques, comme par exemple en bloquant les échanges de mails, nous incitent à choisir leurs services.  
Et tout ça pour quoi ? Pour vos données. Il faut que vous sachiez qu’offrir des services de courriel est un bon moyen de capter / récupérer ces données qui sont l’or noir du XXIe siècle. Ils récupèrent non seulement les données des utilisateur⋅ice⋅s mais aussi (et c’est malheureux) mais celles de leurs correspondant⋅e⋅s, qui n’ont pas l’occasion d’accepter les conditions d’utilisation et l’exploitation des données qui vont avec&nbsp;!

![meme-mail-trump.jpg](img/meme-trump.jpeg)

Nous avons donc de grandes entreprises, qui gèrent nos communications en appliquant leurs règles et en exploitant les données récoltées.

Comment réagiriez-vous si votre facteur vous disait comment écrire, regardait vos échanges pour s'assurer que c'est légitime à ses yeux et en profitait pour relever les informations qu'il pourrait utiliser pour vous envoyer de la publicité&nbsp;?

## Comment sortir de ces prisons dorées ?

À défaut de pouvoir agir directement, il y a une possibilité qui nous est laissée : ne plus recourir à leurs services !

Alors, on vous propose quelques alternatives qui nous semblent tenir la route, d'un point de vue technique et éthique, mais la liste est non-exhaustive&nbsp;!

### Quelques fournisseurs qui nous paraissent dignes de confiance

* **les CHATONS**\
  des structures réunies autour d'une charte portant des valeurs que l'on aime - majoritairement basées en France  
  <https://www.chatons.org/search/by-service?service_type_target_id=112>
* **protonmail**\
  fait un focus sur la vie privée et le secret des correspondances - basé en Suisse\
  <https://proton.me/fr/mail/fr>
* **tutanota**\
  avec, lui aussi, un gros focus sur la sécurité et la vie privée - basé en Allemagne\
  <https://tutanota.com/fr/>
* **gandi.net**\
  un fournisseur de nom de domaine qui intègre 2 adresses emails pour chaque nom de domaine - basé en France\
  <https://www.gandi.net/fr/domain/email>
* **projet CLIC!**\
  l'auto-hébergement de vos emails en s'appuyant sur yunohost - basé chez vous !\
  <https://colibrox.colibris-outilslibres.org>
* Suggestion de ppom : **ecomail** , petit hébergeur en france qui reverse une partie de ses revenus pour des projets écologiques
  <https://www.ecomail.fr/>

Certains de ces services vous demanderont une rétribution financière ou participative. Cette petite contribution permet de garantir la sécurité et la pérennité de vos emails et de ces acteurs éthiques les hébergeant.

### Quelques conseils "écolos" qui peuvent paraître évidents mais qui sont toujours bons à rappeler

* envoyer le moins de courriels possibles (ce mail est-il vraiment utile?)
* avoir une signature de mail sans photo ni logo (et l'équipe Colibris doit aussi cheminer encore la dessus 😅 )
* éviter les pièces-jointes trop lourdes et inutiles (préférer les liens externes vers des espaces de téléchargement temporaires et partager ce lien dans vos messages)
* limiter au maximum les destinataires (cc et cci)
* à intervalle régulier, se désabonner aux lettres d'informations inutiles
* utiliser un logiciel libre de gestion de courriel comme [Thunderbird](https://www.thunderbird.net/fr/) : en plus de gérer par des filtres les messages, vous pourrez archiver vos anciens mails localement, voire configurer votre boite mail pour avoir moins de messages stockés en ligne
* archiver vos dossiers "Boite de réception" et "Éléments envoyés" des années précédentes ailleurs que sur votre hébergement d'emails

![meme brain](img/meme-brain.jpeg)

## Revue de liens

Fournisseurs d’emails, arrêtez de faire de la merde ! (#PasMonCaca) - Framasoft  
<https://framablog.org/2018/08/09/fournisseurs-demails-arretez-de-faire-de-la-merde-pasmoncaca/>  

Être un géant du mail, c’est faire la loi… - Framasoft  
<https://framablog.org/2017/02/17/etre-un-geant-du-mail-cest-faire-la-loi/>  

Le service e-mail de Gandi impacté par un blacklisting de l’organisation « UCEPROTECT » - gandi  
<https://news.gandi.net/fr/2021/03/le-service-e-mail-de-gandi-impacte-par-un-blacklisting-de-organisation-uceprotect/>

Réussir son changement d’adresse e-mail - arobase  
<https://www.arobase.org/messageries/changer-adresse-email.htm>  

Faut-il faire le ménage dans ses mails pour être écolo ? - Tristan Nitot  
<https://www.standblog.org/blog/post/2021/03/25/Faut-il-faire-le-menage-dans-ses-mails-pour-etre-ecolo>

Pourquoi j'ai quitté gmail - Gatien Bataille - Cooptic  
<https://cooptic.be/rien-a-cacher/wakka.php?wiki=QuitterGmail>

Vidéo. Comment les métadonnées permettent de vous surveiller (expliqué en patates) - le Monde  
<https://www.lemonde.fr/pixels/video/2015/06/15/comment-les-metadonnees-permettent-de-vous-surveiller-explique-en-patates_4654461_4408996.html>

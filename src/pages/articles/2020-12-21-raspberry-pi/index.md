---
title: "Des minis ordinateurs pour un Internet plus sobre et plus éthique"
date: "2020-12-21T14:00:00.121Z"
layout: post
path: "/raspberry-pi-mini-ordinateurs-internet-sobre-ethique"
image: ./img/raspberry-pi.jpg
type: blog
categories:
 - raspberry pi
 - résilience
 - numérique éthique
 - circuits courts

---

*Depuis trop longtemps, les GAFAM siphonnent toutes nos données et nous surveillent à des fins commerciales. Heureusement des solutions techniques très locales et écologiques commencent à pointer le bout de leur nez !  En particulier, le [Raspberry pi](https://fr.wikipedia.org/wiki/Raspberry_Pi), un minuscule ordinateur suffisant pour faire sa bureautique, lire ses mails, aller sur Internet et bien sûr, utilise des services libres comme ceux du projet Outils Libres de Colibris.*

## Les nano-ordinateurs à la rescousse

### Qu'est ce que c'est un Raspberry pi ?

Florian, en charge du projet Outils Libres de Colibris, nous l'explique :

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://video.colibris-outilslibres.org/videos/embed/d38a401c-ab56-475a-b9fe-4da7435335c7?warningTitle=0" frameborder="0" allowfullscreen></iframe>

Les usages sont nombreux et variés :
- ordinateur de bureau classique
- serveur web et services type pad
- partagez des publications libres et documents multimédia comme le projet [bibliobox](https://www.bibliobox.net/)
- salle de formation itinérante avec contenus de cours présents sans accès à Internet
- station météo
- diffuseur multimédia de musiques et de films pour son salon
- console de jeux rétro (émulation de consoles des années 90)
- machine pour apprendre à programmer, comprendre Internet
...

**Attention** : n'imaginez pas vous retrouvez avec une machine très puissante vous permettant de faire du montage vidéo ou de la 3d, on est sur un usage sobre : on consomme très peu d'énergie (moins de 5W) et on a un périphérique sans ventilateur (le silence, quel luxe), qui peut être caché dans un coin et se faire oublier.

### Où s'acheter un Raspberry pi ?

Pour l'instant, principalement des boutiques en ligne vendent les Raspberry pi, la boutique française la plus connue étant [Kubii](https://www.kubii.fr/).

Nous vous conseillons de prendre [le modèle Raspberry pi 4 avec au moins 4go de mémoire vive](https://www.kubii.fr/raspberry-pi-4-modele-b/3011-starter-kit-raspberry-pi4-4gb-3272496302112.html) (90 euros avec les câbles et carte SD, sans clavier ni souris).
On trouve aussi des kits avec périphériques claviers et souris ce qui peut s'avérer pratique, comme [ce modèle avec l'ordinateur incrusté dans un clavier](https://www.kubii.fr/raspberry-pi-400/3084-kits-raspberry-pi-400-3272496302914.html).

### Comment installer des logiciels et services dessus

1. Pour commencer il vous faudra, sur un ordinateur de bureau, télécharger le logiciel pour copier des images sur votre carte sd, Raspberry Pi Imager : https://www.raspberrypi.org/software/ puis l'installer (il y a une version windows, mac et linux)
2. Ensuite mettre la carte sd dans votre ordinateur et lancer le programme Raspberry Pi Imager vous pourrez sélectionner une image pré-configurée (par exemple "Raspberry Pi Os")
3. Une fois la copie terminée, il suffit d'insérer la carte dans votre Raspberry pi, et de tout brancher pour utiliser un système linux complet sur de la bureautique!

Pour installer un serveur web avec des services libres, nous vous conseillons la distribution [Yunohost](https://yunohost.org/#/install_on_raspberry) qui permet d'ajouter des services libres comme les pads, les blog wordpress, nextcloud, avec une interface web.

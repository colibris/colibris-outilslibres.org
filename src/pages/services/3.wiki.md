---
title: "Les Wikis"
short: "sites internet collaboratifs"
date: "2018-01-30T22:12:03.284Z"
layout: post
type: services
path: "/services/les-wikis-sites-collaboratifs-yeswiki/"
color: "#d2a564"
image: ./img/Icon-Wikis.svg
description: "Un « wiki » est un site internet dont vous pouvez modifier les pages de façon simple et interactive !"
serviceUrl: https://colibris-wiki.org/?NouvelEspace
serviceUrlTitle: "Créer un wiki"
---

## Qu'est-ce qu'un wiki ?
Un wiki est un site internet d'édition facile, pouvant être utilisé pour documenter un sujet, servir d'aide mémoire, ou rédiger des articles à plusieurs. L'exemple de wiki le plus connu est Wikipedia, encyclopédie co-écrite sur internet. L'édition d'un wiki fonctionne d'une manière très ouverte : tout le monde peut modifier le contenu des pages. Il est aussi possible de revenir en arrière à n'importe quel moment : on garde l'historique des modifications. YesWiki fait partie de la famille des Wikis. Il a la particularité d'être très facile à installer, et d'être en français. C'est un moteur de wiki libre, qui permet de créer et de gérer facilement un site Internet ou intranet. YesWiki est particulièrement destiné aux groupes souhaitant se doter d'un outil pour coopérer via Internet.

## À quoi sert un wiki comme YesWiki ?
- Ça sert à créer facilement un espace internet potentiellement collaboratif
- ça sert à avoir la main sur son site dont le contenu est modifiable d'un simple double-clic
- ça sert à questionner la question du pouvoir
- ça sert à faire des extranets de projets
- ça sert à écrire des livres à plusieurs
- ça sert à monter des projets à plusieurs (et c'est peut-être mieux que de le faire avec les outils de google)
- et ça sert à montrer que les projets à plusieurs sont réalisables et efficaces
- ça sert à démystifier internet
- ça sert à faire des bases de données encore mieux que Google formulaire
- ça sert à repérer les personnes qui souhaitent vraiment coopérer, (les autres disent que n'importe qui va changer leur nom)
- ça sert à repérer les informaticiens qui voient dans la coopération des failles de sécurité
- ça sert à enlever le mot de passe pour pouvoir agir
- ça sert à montrer que wiki peut aussi rimer avec joli
- ça sert à monter en puissance dans ses compétences pour toute l'équipe
- ça sert à faire un site perso d'une façon originale !
- c'est un bout de liberté...

<a class="btn btn-primary px-5" href="https://colibris-wiki.org/?NouvelEspace"><i class="fa fa-plus-circle" aria-hidden="true" style="padding-right: 5px"></i>
 Créer un wiki</a>

<a class="btn btn-primary px-5" href="/avant-de-creer-son-wiki/"><i class="fa fa-lightbulb-o" aria-hidden="true" style="padding-right: 5px"></i>
 Nos préconisations</a>

<a class="btn btn-primary px-5" href="https://colibris-wiki.org/?AnnuaireEspaces"><i class="fa fa-eye" aria-hidden="true" style="padding-right: 5px"></i>
 Voir les wikis existants</a>

## Le logiciel
Les Wiki Outils libres sont basés sur le logiciel libre [YesWiki](https://yeswiki.net).
 
YesWiki est sous licence [GPL 3.0](https://github.com/YesWiki/yeswiki/blob/cercopitheque/LICENSE). 
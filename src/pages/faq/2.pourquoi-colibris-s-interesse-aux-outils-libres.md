---
title: "Pourquoi Colibris s'intéresse aux outils libres ?"
date: "2018-01-01T22:13:03.284Z"
layout: post
type: faq
path: "/faq/pourquoi-colibris-s-interesse-aux-outils-libres/"
categories: 
 - Projet
---

Les valeurs du logiciel libre rejoignent celles d'un mouvement humaniste et écologique comme Colibris. Un ensemble de rencontres et la force de conviction de quelques militants du libre nous ont convaincu de faire notre part sur ce sujet.  
De plus, nous sommes de gros utilisateurs d'outils collaboratifs. Il nous paraissait évident qu'il fallait prendre en main progressivement nos besoins et ne pas surcharger les serveurs d'autres structures. Nous sommes de plus ravis de diffuser ces possibilités à notre écosystème.
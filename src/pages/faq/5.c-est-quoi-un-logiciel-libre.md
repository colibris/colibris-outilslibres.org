---
title: "C'est quoi un logiciel libre ?"
date: "2018-01-30T22:12:03.284Z"
layout: post
type: faq
path: "/faq/c-est-quoi-un-logiciel-libre/"
categories: 
 - Définitions
---

## Les 4 libertés du logiciel libre
Les libertés d'utiliser, de copier, d'étudier et de modifier les logiciels ainsi que de redistribuer les versions modifiées constituent l'essence même du logiciel libre.

Un logiciel est libre si et seulement si sa licence garantit les quatre libertés fondamentales :
 - la liberté d'utiliser le logiciel
 - la liberté de copier le logiciel
 - la liberté d'étudier le logiciel
 - la liberté de modifier le logiciel et de redistribuer les versions modifiées

Concrètement, cela veut dire qu'un logiciel libre peut être utilisé gratuitement, que l'on peut voir son code source et le modifier, en respectant la licence de ce logiciel qui vous impose de mettre a disposition de tous les modifications que vous apportez sous les même conditions de licence.

De ce fait, chaque amélioration du code est accessible a tous.
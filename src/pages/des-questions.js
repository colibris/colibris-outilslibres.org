import React from "react"
import Layout from "../components/layout"
import get from 'lodash/get'
import Helmet from 'react-helmet'
import { graphql } from "gatsby"

class FaqIndex extends React.Component {
  render() {
    const site = get(this, 'props.data.site.siteMetadata')
    const posts = get(this, 'props.data.remark.posts')

    const questions = []

    posts.forEach((data, i) => {
      const layout = get(data, 'post.frontmatter.layout')
      const path = get(data, 'post.path')
      const title = get(data, 'post.frontmatter.title')
      const html = get(data, 'post.html')
      if (layout === 'post' && path !== '/404/') {
        questions.push(
          <div className="question" key={i}>
            <input type="checkbox" defaultChecked />
            <i></i>
            <h3>{title}</h3>
            <p
              className="faq-content"
              dangerouslySetInnerHTML={{ __html: html }}
            />
          </div>
        )
      }
    })

    return (
      <Layout location={this.props.location}>
        <Helmet
          title={get(site, 'title')}
          meta={[
            { name: 'twitter:card', content: 'summary' },
            { name: 'twitter:site', content: `@${get(site, 'twitter')}` },
            { property: 'og:title', content: get(site, 'title') },
            { property: 'og:type', content: 'website' },
            { property: 'og:description', content: get(site, 'description') },
            { property: 'og:url', content: get(site, 'url') },
            {
              property: 'og:image',
              content: `${get(site, 'url')}/img/opengraph.jpg`,
            },
          ]}
        />
        <div className="questions">
          <div className="container">
            <h1 className="underlined">Foire aux questions</h1>
            <div className="accordion">
              {questions}  
            </div>
          </div>
        </div>
      </Layout>
    )
  }
}

export default FaqIndex

export const pageQuery = graphql`
  query FaqQuery {
    site {
      siteMetadata {
        title
        description
        url: siteUrl
        author
        twitter
      }
    }
    remark: allMarkdownRemark(
      filter: {
        frontmatter:{
          type:{eq : "faq"}
        }
      },
      sort: {
        fields: [fileAbsolutePath],
        order: ASC
      }
    ) {
      posts: edges {
        post: node {
          fileAbsolutePath
          html
          excerpt(pruneLength: 300)
          frontmatter {
            layout
            title
            path
            categories
            type
          }
        }
      }
    }
  }
`

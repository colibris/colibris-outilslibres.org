# Colibris Outils Libres

Le code du site <https://colibris-outilslibres.org>.  

![logo](static/img/OutilsLibresScreenshot.png)

Le site web d'Outils Libres est basé sur : [Gatsby starter for bootstrap a blog](https://github.com/jaxx2104/gatsby-starter-bootstrap)

## Pré-requis

Clone git :  
`git clone https://framagit.org/colibris/colibris-outilslibres.org.git`

Install GatsbyJS :  
`npm install --global gatsby-cli`

## Installation des dépendances

```bash
cd colibris-outilslibres.org
npm install
npm rebuild node-sass
```

## Développement local

```bash
cd colibris-outilslibres.org
gatsby develop
```

Préparation au déploiement

```bash
cd colibris-outilslibres.org
gatsby build
```
